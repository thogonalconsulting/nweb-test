#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <libconfig.h>
#include "logger.h"
#include "stringlist.h"
#include "searchrequest.h"
#include "distributed_search.h"

#define VERSION 23
#define MAX_PROXY_URL_LENGTH 255

#ifndef SIGCLD
#define SIGCLD SIGCHLD
#endif

char * validate_request(char * buffer, int fd, int hit){
  long i, ret = 0;

  ret = read(fd,buffer,BUFSIZE);   /* read Web request in one go */
  /* server is single purpose only allow GET of /multiproxy.html */
  if(ret == 0 || ret == -1) {  /* read failure stop now */
    logger(FORBIDDEN,"failed to read browser request","",fd);
  }
  if(ret > 0 && ret < BUFSIZE)  /* return code is valid chars */
    buffer[ret]=0;    /* terminate the buffer */
  else buffer[0]=0;
  logger(LOG,"request",buffer,hit);
  if( strncasecmp(buffer,"get ",4) ) {
    logger(FORBIDDEN,"Only simple GET operation supported",buffer,fd);
  }
  if( strncasecmp(&buffer[4], "/multiproxy.html?", 16) ) {
    logger(NOTFOUND,"Request was not for multiproxy.html or no query",buffer,fd);
  }
  for(i=4;i<BUFSIZE;i++) { /* null terminate after the second space to ignore extra stuff */
    if(buffer[i] == ' ') { /* string is "GET URL " +lots of other stuff */
      buffer[i] = 0;
      break;
    }
  }

  return strdup(&buffer[4]);
}

char * concatenate_results(const StringList_t * results, const char * results_template, int hit){
  static char buffer[BUFSIZE+1];
  static char format_buffer[BUFSIZE+1];
  char * result_buffer = NULL;
  char * start, * end;
  const StringList_t * iterator = results;
  
  /* concatenate all results */
  while(iterator != NULL){
    if(iterator->string != NULL){
      /* each line in a result string is to be treated as a result */
      start = end = iterator->string;
      logger(LOG,"result",iterator->string,hit);
      while( (end = strchr(start, '\n')) ){
        // skip empty lines
        if(end != start){
          sprintf(buffer, "%.*s", (int)(end - start), start);
          // logger(LOG,"buffer",buffer,hit);
          sprintf(format_buffer, results_template, buffer);
          strcat(format_buffer,"\n");
          // logger(LOG,"format_buffer",format_buffer,hit);
          
          if(result_buffer == NULL) 
            result_buffer = strdup(format_buffer);
          else{
            result_buffer = realloc(result_buffer, sizeof(char)*(strlen(result_buffer) + strlen(format_buffer) + 1));
            if(result_buffer == 0)
              logger(ERROR,"Couldn't allocate memory for results", "", hit);
            strcat(result_buffer, format_buffer);
          }
        }
        start = end + 1;
      } 
    }
    iterator = iterator->next;
  }

  return result_buffer;
}

char * format_results(const config_t * cfg, const char * query, const StringList_t * results, int hit){
  char * body_buffer = NULL, * item_list_buffer = NULL;
  const char * body_template = NULL;
  const char * item_template = NULL;
  
  if(config_lookup_string(cfg, "results_template.body", &body_template) 
    && config_lookup_string(cfg, "results_template.result", &item_template)){
      item_list_buffer = concatenate_results(results, item_template, hit);
      if(item_list_buffer == NULL) 
        item_list_buffer = "";
      // logger(LOG,"search results", item_list_buffer, 0);
      body_buffer = malloc(sizeof(char)*(strlen(body_template) + strlen(query) + strlen(item_list_buffer) + 1));
      sprintf(body_buffer, body_template, query, item_list_buffer);
      /* logger(LOG,"body", body_buffer, 0); */
  }

  return body_buffer;
}

/* this is a child web server process, so we can exit on errors */
void web(const config_t cfg, int fd, int hit)
{
  SearchRequest_t * search_request = NULL;
  struct StringList * search_results = NULL;
  char * formatted_results = NULL;
  char * uri_request = NULL;
  static char buffer[BUFSIZE+1]; /* static so zero filled */
  
  uri_request = validate_request(buffer, fd, hit);
  if(uri_request == NULL)
    logger(FORBIDDEN, "Bad Request", buffer, fd);
  else {
    logger(LOG, "Valid Request", uri_request, hit);
    search_request = parse_request(uri_request);    
  }
  
  if(search_request == NULL || search_request->query == NULL || search_request->nodes == NULL)
    logger(FORBIDDEN, "Bad request", "Could not parse query & dest", fd);
  else {
    search_results = execute_search(cfg, search_request, hit);
    logger(LOG,"First search result", search_results->string, hit);

    formatted_results = format_results(&cfg, search_request->query, search_results, hit);

    free(search_results);
    free(search_request->nodes);
    free(search_request->query);
    free(search_request);
  }

  /* send results */
  (void)sprintf(buffer,"HTTP/1.1 200 OK\nServer: server/%d.0\nContent-Length: %ld\nConnection: close\nContent-Type: text/html\n\n", VERSION, strlen(formatted_results)); /* Header + a blank line */
  logger(LOG,"SEND",&buffer[5],hit);
  logger(LOG,"Body",formatted_results, hit);
  (void)write(fd,buffer,strlen(buffer));
  (void)write(fd,formatted_results,strlen(formatted_results));
  
  sleep(1);  /* allow socket to drain before signalling the socket is closed */
  close(fd);
  exit(1);
}

int main(int argc, char **argv)
{
  int i, port, pid, listenfd, socketfd, hit;
  socklen_t length;
  static struct sockaddr_in cli_addr; /* static = initialised to zeros */
  static struct sockaddr_in serv_addr; /* static = initialised to zeros */
  config_t cfg;
  const char *server_cfg_file = "server.cfg";  

  config_init(&cfg);
  if(!config_read_file(&cfg, server_cfg_file))
  {
    fprintf(stderr,"%s:%d - %s\n", config_error_file(&cfg),
            config_error_line(&cfg), config_error_text(&cfg));
    config_destroy(&cfg);
    exit(1);
  }
   
  if( argc < 2  || argc > 2 || !strcmp(argv[1], "-?") ) {
    (void)printf("hint: server Port-Number\n\n"
                  "\tExample: server 8181 &\n\n"
                  "\tOnly Supports Requests for /multiproxy.htm?query=QUERY&dest=a,b,c\n\n");
    exit(0);
  }
  
  logger_name("server.log");  

  /* Become deamon + unstopable and no zombies children (= no wait()) */
  if(fork() != 0)
    return 0; /* parent returns OK to shell */
  (void)signal(SIGCLD, SIG_IGN); /* ignore child death */
  (void)signal(SIGHUP, SIG_IGN); /* ignore terminal hangups */
  for(i=0;i<32;i++)
    (void)close(i);    /* close open files */
  (void)setpgrp();    /* break away from process group */
  logger(LOG,"server starting",argv[1],getpid());
  /* setup the network socket */
  if((listenfd = socket(AF_INET, SOCK_STREAM,0)) <0)
    logger(ERROR, "system call","socket",0);
  port = atoi(argv[1]);
  if(port < 0 || port >60000)
    logger(ERROR,"Invalid port number (try 1->60000)",argv[1],0);
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  serv_addr.sin_port = htons(port);
  if(bind(listenfd, (struct sockaddr *)&serv_addr,sizeof(serv_addr)) <0)
    logger(ERROR,"system call","bind",0);
  if( listen(listenfd,64) <0)
    logger(ERROR,"system call","listen",0);
  for(hit=1; ;hit++) {
    length = sizeof(cli_addr);
    if((socketfd = accept(listenfd, (struct sockaddr *)&cli_addr, &length)) < 0)
      logger(ERROR,"system call","accept",0);
    if((pid = fork()) < 0) {
      logger(ERROR,"system call","fork",0);
    }
    else {
      if(pid == 0) {   /* child */
        (void)close(listenfd);
        web(cfg,socketfd,hit); /* never returns */
      } else {   /* parent */
        (void)close(socketfd);
      }
    }
  }
}
