/**********************************************************
*
*	takes a URI matching our '/multiproxy.htm?dest=A,B,C&query=asdf' format
* 	parses the query string into a search request using uriparser
*
***********************************************************/

#include "searchrequest.h"

#include <stdlib.h>
#include <uriparser/Uri.h>

/* expecting list in form of A,B,C,... */
StringList_t * parse_csv_list(const char * dest_list){
	StringList_t * parsed_list;
	char * tokenizer;
	char * delimiters = ",";
	char * csv_list = strdup(dest_list);

	tokenizer = strtok(csv_list, delimiters);
	while(tokenizer != NULL){
		if(parsed_list == NULL)
			parsed_list = StringList_add(NULL, tokenizer);
		else
			StringList_add(parsed_list, tokenizer);
		tokenizer = strtok(NULL, delimiters);
	}

	return parsed_list;
}

/* return a list of proxy nodes & the search term */
SearchRequest_t * parse_request(const char * uri_request){
  SearchRequest_t * req = NULL;
  UriParserStateA state;
  UriUriA uri;
  UriQueryListA * queryList;
  int itemCount;    
  
  /* now we have the querystring ready to parse */
  state.uri = &uri;
  if(uriParseUriA(&state, uri_request) == URI_SUCCESS) {
    if(uriDissectQueryMallocA(&queryList, &itemCount, uri.query.first, uri.query.afterLast) == URI_SUCCESS){
        UriQueryListA * iterator = queryList;
        req = malloc(sizeof *req);
        req->nodes = NULL;
        req->query = NULL;
        while(iterator != NULL){
          if(strcasecmp(iterator->key,"dest") == 0){
            req->nodes = parse_csv_list(iterator->value); /* logger(LOG,"destination",iterator->value,fd); */         
          }else if(strcasecmp(iterator->key,"query") == 0){            
            req->query = strdup(iterator->value); /* logger(LOG,"query",iterator->value,fd); */
          }
          /* ignore other qs params */
          iterator = iterator->next;
        } 
    }
    uriFreeQueryListA(queryList);
  }
  uriFreeUriMembersA(&uri);

  return req;
}


void free_SearchRequest(SearchRequest_t * request){
  free_StringList(request->nodes);
  free(request->query);
  free(request);
}