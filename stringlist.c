#include <stdlib.h>
#include <string.h>

#include "stringlist.h"

StringList_t * StringList_add(StringList_t * list_node, const char * str){
  StringList_t * new_node = malloc(sizeof * new_node);
  StringList_t * iterator;
  
  new_node->string = strdup(str);
  new_node->next = NULL;
    
  if(list_node != NULL){
    iterator = list_node;
    while(iterator->next != NULL){
      iterator = iterator->next;
    }
    iterator->next = new_node;
  }
  
  return new_node;
}

int StringList_length(const StringList_t * head){
  int count = 0;
  const StringList_t * iterator = head;
  
  while(iterator != NULL){
    iterator = iterator->next;
    count++;
  }

  return count;
}

void free_StringList(StringList_t * head){
  StringList_t * next = head;
  StringList_t * current = head;
  while(next != NULL){
    current = next;
    next = current->next;
    free(current->string);
    free(current);    
  }
}