#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <uriparser/Uri.h>
#include "logger.h"

#define VERSION 23

#ifndef SIGCLD
#define SIGCLD SIGCHLD
#endif
 
char * validate_request(char * buffer, int fd, int hit){
  long i, ret = 0;

  ret = read(fd,buffer,BUFSIZE);   /* read Web request in one go */
  /* server is single purpose only allow GET of /multiproxy.html */
  if(ret == 0 || ret == -1) {  /* read failure stop now */
    logger(FORBIDDEN,"failed to read browser request","",fd);
  }
  if(ret > 0 && ret < BUFSIZE)  /* return code is valid chars */
    buffer[ret]=0;    /* terminate the buffer */
  else buffer[0]=0;
  logger(LOG,"request",buffer,hit);
  if( strncasecmp(buffer,"get ",4) ) {
    logger(FORBIDDEN,"Only simple GET operation supported",buffer,fd);
  }
  if( strncasecmp(&buffer[4], "/query?term=", 12) ) {
    logger(NOTFOUND,"/query?term=",buffer,fd);
  }
  for(i=4;i<BUFSIZE;i++) { /* null terminate after the second space to ignore extra stuff */
    if(buffer[i] == ' ') { /* string is "GET URL " +lots of other stuff */
      buffer[i] = 0;
      break;
    }
  }

  return strdup(&buffer[4]);
}

char * get_query_term(char * request_uri){
  UriParserStateA state;
  UriUriA uri;
  UriQueryListA * queryList;
  int item_count;
  char * search_term = NULL;

  state.uri = &uri;
  if(uriParseUriA(&state, request_uri) == URI_SUCCESS) {
    if(uriDissectQueryMallocA(&queryList, &item_count, uri.query.first, uri.query.afterLast) == URI_SUCCESS){
      UriQueryListA * iterator = queryList;
      while(iterator != NULL){
        if(strcasecmp(iterator->key,"term") == 0){
          search_term = strdup(iterator->value);       
        }
        /* ignore other qs params */
        iterator = iterator->next;
      } 
    }
    uriFreeQueryListA(queryList);
  }
  uriFreeUriMembersA(&uri);

  return search_term;
}

/* get or set the log name */
const char * datafile_name(char * new_name){
  static char log_name[100];

  if(new_name != NULL)
    strcpy(log_name, new_name);
  else if(strlen(log_name) == 0)
    strcpy(log_name, "default.cfg");
  
  return log_name;
}

char * get_query_results(char * query_term, int hit){
  /*  read through the data file to find any lines matching search term
      for simplicity we will read through the data file every time
      we should probably cache this file since it will be read frequently */
  static char buffer[BUFSIZE+1];
  FILE * fp;
  char * lower_case_result;
  char * results = NULL;

  if(query_term == NULL) return results;

  for(int i = 0; query_term[i]; i++){
    query_term[i] = tolower(query_term[i]);
  }  

  fp = fopen(datafile_name(NULL), "r");
  if(fp != NULL){
    while(fgets(buffer, BUFSIZE, fp) != NULL){
      lower_case_result = strdup(buffer);
      for(int i = 0; lower_case_result[i]; i++){
        lower_case_result[i] = tolower(lower_case_result[i]);
      }

      if(strstr(lower_case_result, query_term) != NULL){
        // found a match, add to results
        // sprintf(buffer, "%d size",sizeof(char)*(strlen(buffer)+strlen(results)+1) );
        // logger(LOG, buffer,"boop",-1);
        if(results == NULL){
          results = strdup(buffer);
        }
        else{
          results = realloc(results,sizeof(char)*(strlen(results) + strlen(buffer) + 1));
          if(results == NULL)
            logger(ERROR, "Could not alloc enough memory for results", "", hit);
          strcat(results, buffer);
        }
      } 
    }
  }
  fclose(fp);

  return results;
}

/* this is a child web server process, so we can exit on errors */
void web(int fd, int hit)
{
  char * request_uri;
  char * query_term, * query_result;
  static char buffer[BUFSIZE+1]; /* static so zero filled */
  char * result_to_send = "";
  
  request_uri = validate_request(buffer, fd, hit);
  query_term = get_query_term(request_uri);

  query_result = get_query_results(query_term, hit);
  if(query_result != NULL) result_to_send = query_result;

  logger(LOG,"SEND", request_uri, hit);

  (void)sprintf(buffer,"HTTP/1.1 200 OK\nServer: query_server/%d.0\nContent-Length: %ld\nConnection: close\nContent-Type: text/plain\n\n", VERSION, strlen(result_to_send)); /* Header + a blank line */
  logger(LOG,"Header",buffer,hit);
  (void)write(fd,buffer,strlen(buffer));
  logger(LOG,"Body",result_to_send,hit);
  (void)write(fd,result_to_send,strlen(result_to_send));
  
  free(request_uri);
  free(query_result);
  free(query_term);

  sleep(1);  /* allow socket to drain before signalling the socket is closed */
  close(fd);
  exit(1);
}
 
int main(int argc, char **argv)
{
  int i, port, pid, listenfd, socketfd, hit;
  static char buffer[BUFSIZE];
  socklen_t length;
  static struct sockaddr_in cli_addr; /* static = initialised to zeros */
  static struct sockaddr_in serv_addr; /* static = initialised to zeros */
 
  if( argc < 3  || argc > 3 || !strcmp(argv[1], "-?") ) {
    (void)printf("hint: query_server Port-Number Name\t\tversion %d\n\n"
                  "\tquery_server only serve out matches to /query?term=MYSEARCHTERM"
                  "\t add data to the Name.cfg file\n"
                  "\t all other requests will result in 404\n", VERSION);
                  
    exit(0);
  }

  /* set log name */
  sprintf(buffer, "%s.log", argv[2]);
  logger_name(buffer);
  /* set data file name */
  sprintf(buffer, "%s.cfg", argv[2]);
  datafile_name(buffer);

  /* Become deamon + unstopable and no zombies children (= no wait()) */
  if(fork() != 0)
    return 0; /* parent returns OK to shell */
  (void)signal(SIGCLD, SIG_IGN); /* ignore child death */
  (void)signal(SIGHUP, SIG_IGN); /* ignore terminal hangups */
  for(i=0;i<32;i++)
    (void)close(i);    /* close open files */
  (void)setpgrp();    /* break away from process group */
  logger(LOG," starting",argv[1],getpid());
  /* setup the network socket */
  if((listenfd = socket(AF_INET, SOCK_STREAM,0)) <0)
    logger(ERROR, "system call","socket",0);
  port = atoi(argv[1]);
  if(port < 0 || port >60000)
    logger(ERROR,"Invalid port number (try 1->60000)",argv[1],0);
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  serv_addr.sin_port = htons(port);
  if(bind(listenfd, (struct sockaddr *)&serv_addr,sizeof(serv_addr)) <0)
    logger(ERROR,"system call","bind",0);
  if( listen(listenfd,64) <0)
    logger(ERROR,"system call","listen",0);
  for(hit=1; ;hit++) {
    length = sizeof(cli_addr);
    if((socketfd = accept(listenfd, (struct sockaddr *)&cli_addr, &length)) < 0)
      logger(ERROR,"system call","accept",0);
    if((pid = fork()) < 0) {
      logger(ERROR,"system call","fork",0);
    }
    else {
      if(pid == 0) {   /* child */
        (void)close(listenfd);
        web(socketfd,hit); /* never returns */
      } else {   /* parent */
        (void)close(socketfd);
      }
    }
  }
}
