#ifndef DISTRIBUTED_SEARCH_H
#define DISTRIBUTED_SEARCH_H

#include "stringlist.h"
#include "searchrequest.h"

#include <libconfig.h>

typedef struct UrlResponse UrlResponse_t;

StringList_t * execute_search(const config_t, const SearchRequest_t *, int);

#endif