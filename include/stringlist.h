#ifndef STRINGLIST_H
#define STRINGLIST_H

typedef struct StringList StringList_t;

struct StringList{
  char * string;
  StringList_t * next;
};

StringList_t * StringList_add(StringList_t *, const char *);
int StringList_length(const StringList_t *);
void free_StringList(StringList_t *);

#endif

