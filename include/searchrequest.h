#ifndef SEARCHREQUEST_H
#define SEARCHREQUEST_H

#include "stringlist.h"

typedef struct SearchRequest SearchRequest_t;

struct SearchRequest {
  StringList_t * nodes;
  char * query;
};

SearchRequest_t * parse_request(const char *);
void free_SearchRequest(SearchRequest_t *);

#endif