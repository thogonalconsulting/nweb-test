#ifndef LOGGER_H
#define LOGGER_H

#define BUFSIZE 8096
#define ERROR      42
#define LOG        44
#define FORBIDDEN 403
#define NOTFOUND  404

const char * logger_name(char *);
void logger(int, char *, char *, int);

#endif