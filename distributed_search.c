/**********************************************************
*
*	Performs querying the individual nodes based on a 
* 	search request object, using curl
* 	aggregates results into string list and returns
*
***********************************************************/
#include "distributed_search.h"

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>
#include <pthread.h>
#include <libconfig.h>

#include "logger.h"
#include "stringlist.h"

#define MAX_PROXY_NAME_LENGTH 3

struct UrlResponse {
  char * url;
  struct MemoryStruct * data;
  CURLcode curl_res;
  long reqId;
};

struct MemoryStruct {
  char *memory;
  size_t size;
};

/* use the node name to look up the url in the config */
/* going to arbitrarily limit name to 3 characters since this is unsafe user input*/
char * get_proxy_config_url(const config_t cfg, const char * proxy_name, int hit){
  const char * proxy_url_return;
  char * result = NULL;
  /* proxies.NAME is lookup value in config */
  char * proxy_lookup = malloc(sizeof(char)*(8 + MAX_PROXY_NAME_LENGTH + 1)); 
  
  strcpy(proxy_lookup, "proxies.");
  strncat(proxy_lookup, proxy_name, MAX_PROXY_NAME_LENGTH);
  
  if(config_lookup_string(&cfg, proxy_lookup, &proxy_url_return)){
    result = strdup(proxy_url_return);
  }

  free(proxy_lookup);

  logger(LOG, proxy_lookup, result, hit);  

  return result;
}


static size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
  size_t realsize = size * nmemb;
  struct MemoryStruct *mem = (struct MemoryStruct *)userp;
 
  mem->memory = realloc(mem->memory, mem->size + realsize + 1);
  if(mem->memory == NULL) {
    /* out of memory! */ 
    logger(ERROR,"not enough memory (realloc returned NULL)\n","",0);
    return 0;
  }
 
  memcpy(&(mem->memory[mem->size]), contents, realsize);
  mem->size += realsize;
  mem->memory[mem->size] = 0;
 
  return realsize;
}

static void * get_url_resp(void * url_resp){
  CURL *curl_handle;
  struct UrlResponse * resp = ((struct UrlResponse *)url_resp);
  struct MemoryStruct * chunk = resp->data;
  chunk->memory = malloc(1);  /* will be grown as needed by the realloc above */ 
  chunk->size = 0;    /* no data at this point */ 
  
  curl_handle = curl_easy_init();
  curl_easy_setopt(curl_handle, CURLOPT_URL, resp->url);
  curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
  curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)chunk);
  resp->curl_res = curl_easy_perform(curl_handle);
  curl_easy_cleanup(curl_handle);
 
  return NULL;
}

StringList_t * execute_search(const config_t cfg, const SearchRequest_t * search_request, int hit){
  /*
  StringList_t * result_mock = StringList_add(NULL, strdup("THIS IS FIRST MOCK RESULT"));
  StringList_add(result_mock, strdup("THIS IS SECOND MOCK RESULT"));

  return result_mock;
  */
  static char buffer[BUFSIZE];
  char * node_name;
  StringList_t * node_list = NULL;
  StringList_t * result_list = NULL;
  int num_querynodes, error;
  long i, thread_count;
  struct UrlResponse * curl_result;
  pthread_t * tid;

  node_list = search_request->nodes;
  num_querynodes = StringList_length(node_list);
  curl_result = malloc(num_querynodes * sizeof(struct UrlResponse));
  tid = malloc(num_querynodes * sizeof(pthread_t));
  
  curl_global_init(CURL_GLOBAL_ALL);

  thread_count = 0;
  while(node_list != NULL){
    /* spin up a curl thread for each node to distribute the query */
    /* ignore invalid node names, node names case insensitive */
    sprintf(buffer, "%s", node_list->string);
    for(i = 0; buffer[i]; i++){
      buffer[i] = toupper(buffer[i]);
    }  
    node_name = get_proxy_config_url(cfg, buffer, hit);
    if(node_name != NULL) {
      sprintf(buffer, "%s/query?term=%s", node_name, search_request->query);
      curl_result[thread_count].url = strdup(buffer);
      curl_result[thread_count].data = malloc(sizeof(*(curl_result[thread_count].data)));
      curl_result[thread_count].reqId = thread_count;
      error = pthread_create(&tid[thread_count],
                             NULL,
                             get_url_resp,
                             (void *)&curl_result[thread_count]);
      if(0 != error){
        sprintf(buffer, "Couldn't run thread number %ld, errno %d\n", thread_count, error);
        logger(ERROR, buffer, "", hit);
      }
      else {
        sprintf(buffer, "Thread %ld, gets %s\n", thread_count, curl_result[thread_count].url);
        logger(LOG, buffer, "", hit);
      }

      thread_count++;
    }
    node_list = node_list->next;
  }

  /* now wait for all threads to terminate */ 
  for( i = 0; i < thread_count; i++) {
    error = pthread_join(tid[i], NULL);
    sprintf(buffer, "Thread %ld completed\n", i);
    logger(LOG, buffer, "", hit);
  }

  for( i = 0; i < thread_count; i++){
    /* print results */
    if(curl_result[i].curl_res != CURLE_OK) {
      sprintf(buffer, "%ld failed, res %d", i, curl_result[i].curl_res);
      logger(ERROR, buffer, "", hit); /* not sure if we care if a thread dies along the way */
    }else {
      sprintf(buffer, "Thread %ld: %lu bytes retrieved\n", i, (long)(curl_result[i].data->size));
      logger(LOG, buffer, curl_result[i].data->memory, hit);
      // add results to result list
      StringList_t * temp = StringList_add(result_list, curl_result[i].data->memory);
      if(result_list == NULL)
        result_list = temp;
    }
  }

  /* we're done with libcurl, so clean it up */ 
  /* clean up url result structs */
  curl_global_cleanup();
  free(curl_result);
  free(tid);

  return result_list;
}