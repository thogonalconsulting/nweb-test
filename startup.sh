#!/bin/bash

make clean && make

# need to add a process killer / cleanup

./server 8181 &
./query_server 8182 A &
./query_server 8183 B &
./query_server 8184 C &

# query through the query server
# curl "http://localhost:8181/multiproxy.html?dest=A,B&query=Italian"

# query through individual nodes for testing
# curl "http://localhost:8182/query?term=american"