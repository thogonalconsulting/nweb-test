# README

This project consists of two parts, based on IBM's nweb mini-server

- *server* that receives and distributes HTTP GET requests, in the form 
  */multiproxy.html?dest=NODE,NODE&query=SEARCH* to dedicated query server nodes
- one or more *query_server* instances that search their local data for matches to queries in the form 
  */query?term=SEARCH*


### Pre-requisites for building
* libconfig
* uriparser
* libcurl
```
#Ubuntu Linux 16
sudo apt-get install libcurl4-openssl-dev
sudo apt-get install libconfig-dev
sudo apt-get install liburiparser-dev
```
```
#Mac OS 10.12 (with homebrew)
brew install libconfig
brew install uriparser
# libcurl may already be installed with XCode utilites
brew install curl
```

### Shell command to make and run
```
make clean && make
./server 8181 &
./query_server 8182 A &
./query_server 8183 B &
./query_server 8184 C &
```
### Test the server
```
curl "http://localhost:8181/multiproxy.html?dest=A,B,C&query=Italian"
```

### Test the individual query nodes
```
curl "http://localhost:8182/query?term=american"
```

*Built / tested on Mac OS 10.12 & Ubuntu Linux 16.10, 1 server node / 3 query server nodes*

### Assumptions
- server instances ignore irrelevant querystring values, invalid destination names, will still process any correct querystring parameters
- query server (dest) names are 1 to 3 alphanumeric long
- each line in query server data file is a record
- query terms are one word 
- blank lines are ignored in results
- data is partitioned among the query nodes
otherwise would need to add a unique id to record for de-dupe purposes
### Future Improvements
- query nodes read full data file every query; this is not ideal for performance. consider caching files in memory or use external caching mechanism
- should add a record schema or delimiter other than newlines for data files
- dedupe results / dont require disjoint sets for data
- havent tested with multiple words query terms / didnt add URL decoding


### File Details
##### Configs
- server.cfg - 
holds the result template for the body and the individual results
also holds a list of valid query servers to distribute requests to
- A/B/C.cfg - 
data files that hold records to be searched
one line per record, blank records are ignored
assumed to be unique records in each node, there is no de-dupe on the server
##### Code - Common
- logger.c/h - 
extracted from the nweb server to handle logging for both server and query server
##### Code - Server
- server.c - 
receives an HTTP GET request of the form /multiproxy.html?dest=A,B&query=SEARCH, 
parses the request into a list of nodes (A and B in this case) and forwards the query to the nodes. 
Looks up URL for the node name in the query server, pass the query along to each destination node in parallel
Treats each newline from the query server as a result record.
- stringlist.c/h
simple linked list (partial) implementation for lists of strings
- search_request.c/h - 
data structure and code to parse server requests into data to send to downstream query servers
- distributed_search.c/h - 
wraps libcurl and parallelizes requests to query servers; based on multithread example on libcurl site

##### Code - Query Server
- query_server.c - 
receives a request, parses querystring for '/query?term=SEARCH', and performs case insensitive search in the query server's data file
returns all lines in file that contain a match