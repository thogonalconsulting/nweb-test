#template from http://www.cs.colby.edu/maxwell/courses/tutorials/maketutor/makefile.5
IDIR=-I/usr/local/include -I./include
LDIR=-L/usr/local/lib
CC=gcc -ggdb -Wall
CFLAGS=$(IDIR)
LFLAGS=$(LDIR)
ODIR=obj
MKDIR_P = mkdir -p

#_DEPS = hellomake.h
#DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

#_OBJS = server.o proxy.o
#OBJ = $(patsubst %,$(ODIR)/%,$(_OBJS))

all: directories server query_server
.PHONY: all directories

directories:
	$(MKDIR_P) $(ODIR)

$(ODIR)/%.o: %.c $(DEPS) directories 
	$(CC) -c -o $@ $< $(CFLAGS)

server: obj/server.o obj/stringlist.o obj/searchrequest.o obj/distributed_search.o obj/logger.o
	gcc -o $@ $^ $(CFLAGS) $(LFLAGS) -lconfig -lcurl -luriparser -lpthread

query_server: obj/query_server.o obj/logger.o
	gcc -o $@ $^ $(CFLAGS) $(LFLAGS) -luriparser

clean:
	rm -f $(ODIR)/*.o *~ core $(INCDIR)/*~ 
